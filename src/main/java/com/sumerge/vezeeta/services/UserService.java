package com.sumerge.vezeeta.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.sumerge.vezeeta.models.Specialty;
import com.sumerge.vezeeta.models.User;
import com.sumerge.vezeeta.repositories.UsersRepository;

@Service
public class UserService {

	@Autowired
	private UsersRepository usersRepository;
	
	@Transactional
	public User findById(int id) {
		User user=usersRepository.getById(id);
		return user;
	}

	@Transactional
	public void deleteByUser(User user) {
		usersRepository.delete(user);
		
	}

	@Transactional
	public void save(User user) {
		usersRepository.save(user);
		
	}

	@Transactional
	public List<User> findAll() {
		List<User> users=usersRepository.findAll();
		return users;
	}
	
	public List<User> findBySpecialty(Specialty specialty){
		return null;
	}
	
	public List<User> findByName(String name){
		List<User> a=this.usersRepository.findByFirstName(name);
		List<User> b=this.usersRepository.findByFirstName(name);
		a.addAll(b);
		return a;
	}
	
	public List<User> findByArea(String area){
		return usersRepository.findByArea(area);
	}
	
	public List<User> findDoctors(){
		return usersRepository.findByRoles("ROLE_DOCTOR");
	}
	
	public List<User> findPatients(){
		return usersRepository.findByRoles("ROLE_USER");
	}

}
