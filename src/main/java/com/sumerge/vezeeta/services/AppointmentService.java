package com.sumerge.vezeeta.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.sumerge.vezeeta.models.Appointment;
import com.sumerge.vezeeta.models.User;
import com.sumerge.vezeeta.repositories.AppointmentsRepository;

@Service
public class AppointmentService {

	@Autowired
	private AppointmentsRepository appointmentsRepository;
	
	
	
	@Transactional
	public Appointment findById(int id) {
		Appointment appointment=appointmentsRepository.getById(id);
		return appointment;
	}

	@Transactional
	public void deleteByAppointment(Appointment appointment) {
		appointmentsRepository.delete(appointment);
		
	}

	@Transactional
	public void save(Appointment appointment) {
		appointmentsRepository.save(appointment);
		
	}

	@Transactional
	public List<Appointment> findAll() {
		List<Appointment> appointments=appointmentsRepository.findAll();
		return appointments;
	}
	
	@Transactional
	public void bookAppointment(Appointment appointment,String comment, User patient) {
		appointment.setIsBooked(true);
		appointment.setComments(comment);
		appointment.setPatientId(patient);
		patient.addRecievedAppointments(appointment);
	}
	
	@Transactional
	public void unBookAppointment(Appointment appointment) {
		appointment.setIsBooked(false);
		appointment.setComments(null);
		appointment.getPatientId().getAppointmentsRecieved().remove(appointment);
		appointment.setPatientId(null);
	
	}

}
