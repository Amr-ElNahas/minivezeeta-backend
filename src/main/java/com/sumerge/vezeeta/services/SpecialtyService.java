package com.sumerge.vezeeta.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.sumerge.vezeeta.models.Specialty;
import com.sumerge.vezeeta.repositories.SpecialtiesRepository;

@Service
public class SpecialtyService {

	@Autowired
	private SpecialtiesRepository specialtiesRepository;
	
	@Transactional
	public Specialty findById(int id) {
		Specialty specialty=specialtiesRepository.getById(id);
		return specialty;
	}
	@Transactional
	public void deleteBySpecialty(Specialty specialty) {
		specialtiesRepository.delete(specialty);
		
	}

	@Transactional
	public void save(Specialty specialty) {
		specialtiesRepository.save(specialty);
		
	}
	@Transactional
	public List<Specialty> findAll() {
		List<Specialty> specialties=specialtiesRepository.findAll();
		return specialties;
	}

}
