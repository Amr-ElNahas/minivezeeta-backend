package com.sumerge.vezeeta.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.sumerge.vezeeta.models.Specialty;
import com.sumerge.vezeeta.services.SpecialtyService;

@RestController
@RequestMapping("/api")
public class SpecialtyController {
	private SpecialtyService specialtyService;
	
	@Autowired
	public SpecialtyController(SpecialtyService specialtyService) {
		this.specialtyService=specialtyService;
	}
	
	@GetMapping("/specialties")
	public List<Specialty> findAll(){
		return specialtyService.findAll();
	}
	
	@GetMapping("/specialties/{id}")
	public Specialty findById(@PathVariable int id){
		Specialty specialty= specialtyService.findById(id);
		if(specialty==null) {
			throw new RuntimeException("Specialty not found "+id);
		}
		return specialty;
	}
	
	@PostMapping("/specialties")
	public Specialty AddSpecialty(@RequestBody Specialty specialty) {
		specialty.setId(0);
		specialtyService.save(specialty);
		return specialty;
	}
	
	@PutMapping("/specialties")
	public Specialty updateSpecialty(@RequestBody Specialty specialty) {
		specialtyService.save(specialty);
		return specialty;
	}
	@DeleteMapping("/specialties/{id}")
	public String deleteSpecialty(@PathVariable int id) {
		Specialty specialty=specialtyService.findById(id);
		if(specialty==null) {
			throw new RuntimeException("Specialty not found");
		}
		specialtyService.deleteBySpecialty(specialty);
		return "Deleted specialty - "+id;
	}
}
