package com.sumerge.vezeeta.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.sumerge.vezeeta.models.User;
import com.sumerge.vezeeta.services.UserService;

@RestController
@RequestMapping("/api")
public class UserController {
	private UserService userService;
	
	@Autowired
	public UserController(UserService userService) {
		this.userService=userService;
	}
	
	
	//CRUD FOR WHOLE USER
	
	@GetMapping("/users")
	public List<User> findAll(){
		return userService.findAll();
	}
	
	@GetMapping("/users/{id}")
	public User findById(@PathVariable int id){
		User user= userService.findById(id);
		if(user==null) {
			throw new RuntimeException("User not found "+id);
		}
		return user;
	}
	
	@PostMapping("/users")
	public User AddUser(@RequestBody User user) {
		user.setId(0);
		user.setActive(true);
		userService.save(user);
		return user;
	}
	
	@PutMapping("/users")
	public User updateUser(@RequestBody User user) {
		userService.save(user);
		return user;
	}
	@DeleteMapping("/users/{id}")
	public String deleteUser(@PathVariable int id) {
		User user=userService.findById(id);
		if(user==null) {
			throw new RuntimeException("User not found");
		}
		userService.deleteByUser(user);
		return "Deleted user - "+id;
	}
	
	//CRUD FOR DOCTOR
	
	@GetMapping("/doctors")
	public List<User> findAllDoctors(){
		return userService.findDoctors();
	}
	
	@GetMapping("/doctors/{id}")
	public User findDoctorById(@PathVariable int id){
		User user= userService.findById(id);
		if(user==null) {
			throw new RuntimeException("User not found "+id);
		}
		if(!user.getRoles().equals("ROLE_DOCTOR")) {
			throw new RuntimeException("No Doctor With Current ID");
		}
		return user;
	}
	
	@PostMapping("/doctors")
	public User AddDoctor(@RequestBody User user) {
		user.setId(0);
		user.setRoles("ROLE_DOCTOR");
		user.setActive(true);
		userService.save(user);
		return user;
	}
	
	@PutMapping("/doctors")
	public User updateDoctor(@RequestBody User user) {
		if(!user.getRoles().equals("ROLE_DOCTOR")) {
			throw new RuntimeException("Not allowed to change access");
		}
		userService.save(user);
		return user;
	}
	@DeleteMapping("/doctors/{id}")
	public String deleteDoctor(@PathVariable int id) {
		User user=userService.findById(id);
		if(user==null) {
			throw new RuntimeException("User not found");
		}
		userService.deleteByUser(user);
		return "Deleted user - "+id;
	}
	
	//CRUD FOR Patient
	
	@GetMapping("/patients")
	public List<User> findAllPatients(){
		return userService.findDoctors();
	}
	
	@GetMapping("/patients/{id}")
	public User findPatientById(@PathVariable int id){
		User user= userService.findById(id);
		if(user==null) {
			throw new RuntimeException("User not found "+id);
		}
		if(!user.getRoles().equals("ROLE_USER")) {
			throw new RuntimeException("No User With Current ID");
		}
		return user;
	}
	
	@PostMapping("/patients")
	public User AddPatient(@RequestBody User user) {
		user.setId(0);
		user.setRoles("ROLE_USER");
		user.setActive(true);
		user.setEntityName(null);
		user.setEntity(null);
		user.setFees(null);
		userService.save(user);
		return user;
	}
	
	@PutMapping("/patients")
	public User updatePatient(@RequestBody User user) {
		if(!user.getRoles().equals("ROLE_USER")) {
			throw new RuntimeException("Not allowed to change access");
		}
		userService.save(user);
		return user;
	}
	@DeleteMapping("/patients/{id}")
	public String deletePatient(@PathVariable int id) {
		User user=userService.findById(id);
		if(user==null) {
			throw new RuntimeException("User not found");
		}
		userService.deleteByUser(user);
		return "Deleted user - "+id;
	}
}
