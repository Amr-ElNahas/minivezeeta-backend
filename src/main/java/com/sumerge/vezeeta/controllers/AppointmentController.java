package com.sumerge.vezeeta.controllers;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.sumerge.vezeeta.models.Appointment;
import com.sumerge.vezeeta.models.User;
import com.sumerge.vezeeta.services.AppointmentService;
import com.sumerge.vezeeta.services.UserService;

@RestController
@RequestMapping("/api")
@CrossOrigin(origins = {"http://localhost:8082","http://localhost:4200"})
public class AppointmentController {
	private AppointmentService appointmentService;
	private UserService userService;
	
	@Autowired
	public AppointmentController(AppointmentService appointmentService,UserService userService) {
		this.appointmentService=appointmentService;
		this.userService=userService;
	}
	
	
	//GET ALL APPOINTMENTS
	@GetMapping("/appointments")
	public List<Appointment> findAll(){
		List<Appointment> appointments=appointmentService.findAll();
		return appointments;
	}
	//GET SPECIFIC APPOINTMENT
	@GetMapping("/appointments/{id}")
	public Appointment findById(@PathVariable int id){
		Appointment appointment= appointmentService.findById(id);
		if(appointment==null) {
			throw new RuntimeException("Appointment not found "+id);
		}
		return appointment;
	}
	//GET ALL APPOINTMENTS THAT THE DOCTOR HAS
	@GetMapping("/appointments/doctors/{id}")
	public List<Appointment> findAllDoctorAppointment(@PathVariable int id){
		List<Appointment> appointments=userService.findById(id).getAppointmentsSet();
		return appointments;
	}
	
	//GET ALL APPOINTMENTS THAT THE Patient HAS
	@GetMapping("/appointments/patients/{id}")
	public List<Appointment> findAllPatientAppointment(@PathVariable int id){
		List<Appointment> appointments=userService.findById(id).getAppointmentsRecieved();
		return appointments;
	}
	//BOOK APPOINTMENT
	@PutMapping("/appointments/patients/{pid}/book")
	public String bookAppointment(@RequestBody Appointment appointmentBody,@PathVariable int pid) {
		System.out.println(appointmentBody);
		Appointment appointment=this.appointmentService.findById(appointmentBody.getId());
		if(appointment.getIsBooked()) {
			throw new RuntimeException("appointment is already booked");
		}
		User patient = userService.findById(pid);
		String comment=appointmentBody.getComments();
		appointmentService.bookAppointment(appointment, comment, patient);
		return "booked successfully appointment at "+appointment.getDateTime()+" with "+appointment.getDoctorId().getFirstName()+" "+appointment.getDoctorId().getLastName();
	}
	//UNBOOK APPOINTMENT
	@PutMapping("/appointments/patients/{pid}/unbook")
	public String unBookAppointment(@RequestBody Appointment appointmentBody,@PathVariable int pid) {
		Appointment appointment=this.appointmentService.findById(appointmentBody.getId());
		if(appointment.getPatientId().getId()!=pid) {
			throw new RuntimeException("this is not allowed");
		}
		appointmentService.unBookAppointment(appointment);
		return "unbooked successfully appointment at "+appointment.getDateTime()+" with "+appointment.getDoctorId().getFirstName()+" "+appointment.getDoctorId().getLastName();
	}
	//CREATE APPOINTMENT
	@PostMapping(value = "/appointments" )
	public Appointment AddAppointment(@RequestBody Appointment appointment) {
		appointment.setId(0);
		appointmentService.save(appointment);
		return appointment;
	}
	
	//DOCTOR CREATING APPOINTMENT FOR HIMSELF
	@PostMapping(value = "/appointments/doctors/{id}" )
	public Appointment AddAppointmentByDoctor(@RequestBody Appointment appointment, @PathVariable int id) {
		User user = userService.findById(id);
		if(user==null) {
			throw new RuntimeException("not found");
		}
		appointment.setId(0);
		appointment.setDoctorId(user);
		appointment.setIsBooked(false);
		appointment.setPatientId(null);
		appointment.setComments(null);
		user.getAppointmentsSet().add(appointment);
		appointmentService.save(appointment);
		return appointment;
	}
	
	//UPDATING TIME FOR THE APPOINTMENT
	@PutMapping("/appointments/updatetime")
	public Appointment updateTimingOfAppointment(@RequestBody Appointment appointment) {
		Appointment app= appointmentService.findById(appointment.getId());
		app.setDateTime(appointment.getDateTime());
		appointmentService.save(app);
		return app;
	}
	
	//UPDATING WHOLE APPOINTMENT DETAILS
	@PutMapping("/appointments")
	public Appointment updateAppointment(@RequestBody Appointment appointment) {
		appointmentService.save(appointment);
		return appointment;
	}
	
	//DELETING APPOINTMENTS
	@DeleteMapping("/appointments/{id}")
	public String deleteAppointment(@PathVariable int id) {
		Appointment appointment=appointmentService.findById(id);
		if(appointment==null) {
			throw new RuntimeException("Appointment not found");
		}
		appointmentService.deleteByAppointment(appointment);
		return "Deleted appointment - "+appointment;
	}
}
