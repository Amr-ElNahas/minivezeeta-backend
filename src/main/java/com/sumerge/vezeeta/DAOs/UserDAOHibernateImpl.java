//package com.sumerge.vezeeta.DAOs;
//
//import java.util.List;
//
//import javax.persistence.EntityManager;
//
//import org.hibernate.query.NativeQuery;
//import org.hibernate.Session;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Repository;
//
//import com.sumerge.vezeeta.models.User;
//
//
//@Repository
//public class UserDAOHibernateImpl implements UserDAO {
//	
//	private  EntityManager entityManager;
//	
//	@Autowired
//	public UserDAOHibernateImpl(EntityManager entityManager) {
//		this.entityManager=entityManager;
//	}
//	
//	@Override
//	public List<User> findAll() {
//		Session session=entityManager.unwrap(Session.class);
//		NativeQuery<User> query=session.createNativeQuery("select * from User",User.class);
//		return query.getResultList();
//	}
//
//	@Override
//	public User findById(int id) {
//		Session session=entityManager.unwrap(Session.class);
//		//NativeQuery<User> query=session.createNativeQuery("select * from User e where e.id="+id,User.class);
//		//return query.getSingleResult();
//		return session.get(User.class, id);
//	}
//
//	@Override
//	public void save(User user) {
//		Session session=entityManager.unwrap(Session.class);
//		session.saveOrUpdate(user);
//	}
//
//	@Override
//	public void deleteById(int id) {
//		Session session=entityManager.unwrap(Session.class);
//		session.delete(findById(id));
//	}
//
//}
