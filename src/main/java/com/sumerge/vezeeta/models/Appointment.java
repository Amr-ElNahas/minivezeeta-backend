package com.sumerge.vezeeta.models;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIdentityReference;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

@Entity
@Table(name="appointments")
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")

public class Appointment {
	
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private int id;
	
	@Temporal(TemporalType.TIMESTAMP)
	@JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
	@Column(name="date")
	private Date dateTime;
	
	@ManyToOne(fetch = FetchType.LAZY, cascade = {CascadeType.DETACH,CascadeType.MERGE,CascadeType.PERSIST,CascadeType.REFRESH})
	@JoinColumn(name="doctor_id")
	@JsonIdentityReference(alwaysAsId = true)
	private User doctorId;
	
	@Column(name="comments")
	private String comments;
	@Column(name="is_booked")
	private Boolean isBooked;
	
	@ManyToOne(fetch = FetchType.LAZY, cascade = {CascadeType.DETACH,CascadeType.MERGE,CascadeType.PERSIST,CascadeType.REFRESH})
	@JoinColumn(name="patient_id")
	@JsonIdentityReference(alwaysAsId = true)
	private User patientId;

	public Appointment() {
	}
	
	

	public Appointment(Date dateTime, User doctorId, String comments, Boolean isBooked) {
		this.dateTime = dateTime;
		this.doctorId = doctorId;
		this.comments = comments;
		this.isBooked = isBooked;
	}

	

	public Appointment(Date dateTime, User doctorId) {
		this.dateTime = dateTime;
		this.doctorId = doctorId;
	}



	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Date getDateTime() {
		return dateTime;
	}

	public void setDateTime(Date dateTime) {
		this.dateTime = dateTime;
	}

	public User getDoctorId() {
		return doctorId;
	}

	public void setDoctorId(User doctorId) {
		this.doctorId = doctorId;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public Boolean getIsBooked() {
		return isBooked;
	}

	public void setIsBooked(Boolean isBooked) {
		this.isBooked = isBooked;
	}

	public User getPatientId() {
		return patientId;
	}

	public void setPatientId(User patientId) {
		this.patientId = patientId;
	}



	@Override
	public String toString() {
		return "Appointment [id=" + id + ", dateTime=" + dateTime + ", doctorId=" + doctorId + ", comments=" + comments
				+ ", isBooked=" + isBooked + ", patientId=" + patientId + "]";
	}

	
	
	
}
