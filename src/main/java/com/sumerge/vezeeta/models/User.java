package com.sumerge.vezeeta.models;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIdentityReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;


@Entity
@Table(name="users")
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")

public class User {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Integer id;
	
	@Column(name="active")
	private Boolean active;
	
	@Column(name="password")
	private String password;
	
	@Column(name="roles")
	private String roles;
	
	@Column(name="email")
	private String username;
	
	@Column(name="area")
	private String area;
	
	@Column(name = "date_of_birth")
	@Temporal(TemporalType.DATE)
	@JsonFormat(pattern="yyyy-MM-dd")
	private Date dateOfBirth;
	
	@Column(name = "entity")
	private String entity;
	
	@Column(name = "entity_name")
	private String entityName;
	
	@Column(name="first_name")
	private String firstName;
	
	@Column(name="last_name")
	private String lastName;
	
	@Column(name = "fees")
	private Float fees;
	
	@Column(name="gender")
	private String gender;
	
	@Column(name="phone")
	private String phoneNumber;
	
	@Column(name="waiting_time")
	@Temporal(TemporalType.TIME)
	private Date waitingTime;
	
	@ManyToMany(fetch = FetchType.LAZY,cascade = {CascadeType.DETACH,CascadeType.MERGE,CascadeType.PERSIST,CascadeType.REFRESH})
	@JoinTable(
			name = "doctor_specialty",
			joinColumns=@JoinColumn(name="doctor_id"),
			inverseJoinColumns = @JoinColumn(name="specialty_id")
			)
	
	private List<Specialty> specialties;
	
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "doctorId", cascade = {CascadeType.ALL})
	//@JsonIdentityReference(alwaysAsId = true)
	private List<Appointment> appointmentsSet; //APPOINTMENTS THAT ARE SET BY DOCTOR
	
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "patientId", cascade = {CascadeType.DETACH,CascadeType.MERGE,CascadeType.PERSIST,CascadeType.REFRESH})
	@JsonIdentityReference(alwaysAsId = true)
	private List<Appointment> appointmentsRecieved; //APPOINTMENTS THAT ARE SET BY PATIENT


	public User(Boolean active, String password, String roles, String username, String area, Date dateOfBirth,
			String firstName, String lastName, String gender, String phoneNumber) {
		this.active = active;
		this.password = password;
		this.roles = roles;
		this.username = username;
		this.area = area;
		this.dateOfBirth = dateOfBirth;
		this.firstName = firstName;
		this.lastName = lastName;
		this.gender = gender;
		this.phoneNumber = phoneNumber;
	}

	public User(Boolean active, String password, String roles, String username, String area, Date dateOfBirth,
			String entity, String entityName, String firstName, String lastName, Float fees, String gender,
			String phoneNumber, Date waitingTime) {
		this.active = active;
		this.password = password;
		this.roles = roles;
		this.username = username;
		this.area = area;
		this.dateOfBirth = dateOfBirth;
		this.entity = entity;
		this.entityName = entityName;
		this.firstName = firstName;
		this.lastName = lastName;
		this.fees = fees;
		this.gender = gender;
		this.phoneNumber = phoneNumber;
		this.waitingTime = waitingTime;
	}

	public User() {
	}
	
	

	public String getArea() {
		return area;
	}

	public void setArea(String area) {
		this.area = area;
	}

	public Date getDateOfBirth() {
		return dateOfBirth;
	}

	public void setDateOfBirth(Date dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	public String getEntity() {
		return entity;
	}

	public void setEntity(String entity) {
		this.entity = entity;
	}

	public String getEntityName() {
		return entityName;
	}

	public void setEntityName(String entityName) {
		this.entityName = entityName;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public Float getFees() {
		return fees;
	}

	public void setFees(Float fees) {
		this.fees = fees;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public Date getWaitingTime() {
		return waitingTime;
	}

	public void setWaitingTime(Date waitingTime) {
		this.waitingTime = waitingTime;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Boolean isActive() {
		return active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getRoles() {
		return roles;
	}

	public void setRoles(String roles) {
		this.roles = roles;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}
	

	public List<Specialty> getSpecialties() {
		return specialties;
	}

	public void setSpecialties(List<Specialty> specialties) {
		this.specialties = specialties;
	}
	
	public void addSpecialties(Specialty specialty) {
		if(specialties==null) {
			specialties=new ArrayList<Specialty>();
		}
		specialties.add(specialty);
	}
	
	public void removeSpecialties(Specialty specialty) {
		if(specialties==null) {
			specialties.remove(specialty);
		}
	}
	

	public List<Appointment> getAppointmentsSet() {
		return appointmentsSet;
	}

	public void setAppointmentsSet(List<Appointment> appointmentsSet) {
		this.appointmentsSet = appointmentsSet;
	}

	public List<Appointment> getAppointmentsRecieved() {
		return appointmentsRecieved;
	}

	public void setAppointmentsRecieved(List<Appointment> appointmentsRecieved) {
		this.appointmentsRecieved = appointmentsRecieved;
	}
	
	public void addSetAppointments(Appointment appointment) {
		if(appointmentsSet==null) {
			appointmentsSet=new ArrayList<Appointment>();
		}
		appointmentsSet.add(appointment);
	}
	
	public void removeSetAppointments(Appointment appointment) {
		appointmentsSet.remove(appointment);
	}
	
	public void addRecievedAppointments(Appointment appointment) {
		if(appointmentsRecieved==null) {
			appointmentsRecieved=new ArrayList<Appointment>();
		}
		appointmentsRecieved.add(appointment);
	}
	public void removeRecievedAppointments(Appointment appointment) {
		appointmentsRecieved.remove(appointment);
	}
	
	

	@Override
	public String toString() {
		return "User [id=" + id + ", active=" + active + ", password=" + password + ", roles=" + roles + ", username="
				+ username + ", area=" + area + ", dateOfBirth=" + dateOfBirth + ", entity=" + entity + ", entityName="
				+ entityName + ", firstName=" + firstName + ", lastName=" + lastName + ", fees=" + fees + ", gender="
				+ gender + ", phoneNumber=" + phoneNumber + ", waitingTime=" + waitingTime + ", specialties="
				+ specialties + ", appointmentsSet=" + appointmentsSet + ", appointmentsRecieved="
				+ appointmentsRecieved + "]";
	}
	
	

	

	
	
}
