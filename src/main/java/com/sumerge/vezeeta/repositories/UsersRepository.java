package com.sumerge.vezeeta.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.sumerge.vezeeta.models.User;

public interface UsersRepository extends JpaRepository<User, Integer> {
	public List<User> findByFirstName(String firstName);
	public List<User> findByLastName(String lastName);
	public List<User> findByArea(String area);
	public List<User> findByRoles(String roles);
}
