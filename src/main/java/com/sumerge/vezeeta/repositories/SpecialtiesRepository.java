package com.sumerge.vezeeta.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.sumerge.vezeeta.models.Specialty;

public interface SpecialtiesRepository extends JpaRepository<Specialty, Integer> {

}
