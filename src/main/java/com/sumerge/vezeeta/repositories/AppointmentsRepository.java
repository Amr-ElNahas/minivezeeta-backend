package com.sumerge.vezeeta.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.sumerge.vezeeta.models.Appointment;

public interface AppointmentsRepository extends JpaRepository<Appointment, Integer> {

}
